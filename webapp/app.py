from flask import Flask
from flask import Response
from flask import request
from query_db import query_db
from update_db import update_db

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return "Hello! You can use the /query_db and /update_db?msg=xxx URLs"

@app.route('/query_db', methods=['GET'])
def get_message():
  return query_db()

@app.route('/update_db', methods=['GET'])
def update_message():
  msg = request.args.get('msg')
  print("Recording this message: "+msg)
  return update_db(msg)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)


