
# Oracle Cloud Infrastrucutre Demo
Showcase of the Oracle Cloud Infrastructure (OCI) capabilities in a 15min demo.

During this demo will will leverage the OCI orchestration capabilities to build the following architecture:
![architecture.png](architecture/architecture.png)



# Requirements
- an OCI account
- OCI CLI 
- terraform installed and configured for OCI

In this section we give more details on how to install these requirements. Feel free to jump to the next section if your environment already meets these requirements.

## OCI account
If you do not have an account already, you can request a trial account from this page:
https://www.oracle.com/cloud/free/

### Install and configure the CLI
We install the CLI before Terraform so we can conveniently generate an API Key and make sure we can authenticate to OCI.

Follow the official instructions to install the OCI CLI. On Mac OS X you can simply use Homebrew:
```
brew update && brew install oci-cli
oci setup config
# accept all defaults and provide requested information from the OCI console
```

Upload the public key to the OCI console in your `User Settings` and check the MD5 matches.

### Create a compartment
A compartment is high-level container that can be used to isolate a group of resources on OCI.
Create a compartment (or sub-compartment) in the console and add its OCID at the end of the `~/.oci/config` configuration file:
```
compartment-id=<your_compartment_ocid>
```

Change the permission of the config file and check the CLI is working:
```
chmod 600 ~/.oci/config
oci iam user list
```

### Install and configure Terraform
First make sure Terraform is installed. The provided terraform scripts have been tested with version 0.14:
```
terraform --version
Terraform v0.14.8
```

Modify the variables from the `terraform.tfvars` file to match those from `~/.oci/config`.
Initialize Terraform:
```
cd terraform
terraform init
```

# OCI deployment with Terraform
```
terraform validate
terraform plan
terraform apply
```

The deployment should take around 5 minutes to complete.

### Create SSH tunnel through bastion host
```
. ./connect_bastion.sh
```

### Transfer wallet, initialiaze DB and start webapp
```
scp -o StrictHostKeyChecking=no -P 8022 autonomous_database_wallet.zip ../webapp/* opc@127.0.0.1:
ssh -o StrictHostKeyChecking=no -p 8022 opc@localhost
. ./start_app.sh 
```
You should see the incoming healthcheck queries from the load balancer.

### Connect to webapp from an Internet browser
Use the `application-query-url` and `application-update-url` URLs from the terraform output to interact with the application.


# Clean-up
```
terraform destroy
```
This will take around 5 minutes.


