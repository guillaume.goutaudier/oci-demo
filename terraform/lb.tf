# ------ Public Load Balancer
resource "oci_load_balancer_load_balancer" "load_balancer" {
    compartment_id = var.compartment_ocid
    display_name = "lb"
    shape = "100Mbps"
    subnet_ids = [oci_core_subnet.public-subnet.id]
    is_private = "false"
}
output "application-query-url" {
  value = "http://${oci_load_balancer_load_balancer.load_balancer.ip_address_details[0].ip_address}/query_db"
}
output "application-update-url" {
  value = "http://${oci_load_balancer_load_balancer.load_balancer.ip_address_details[0].ip_address}/update_db?msg=your_message"
}

# ------ Load Balancer Backend Set
resource "oci_load_balancer_backend_set" "backend_set" {
    health_checker {
        protocol = "HTTP"
        interval_ms = 5000
        port = 5000
        retries = 3
        return_code = 200
        timeout_in_millis = 1000
        url_path = "/"
    }
    load_balancer_id = oci_load_balancer_load_balancer.load_balancer.id
    name = "app_backend_set"
    policy = "LEAST_CONNECTIONS"
    session_persistence_configuration {
        cookie_name = "oci_lb_cookie"
    }
}

# ------ Load Balancer Backend Server
resource "oci_load_balancer_backend" "load_balancer_backend" {
    backendset_name = oci_load_balancer_backend_set.backend_set.name
    ip_address = oci_core_instance.webapp.private_ip
    load_balancer_id = oci_load_balancer_load_balancer.load_balancer.id
    port = 5000
}

# ------ Load Balancer Listener
resource "oci_load_balancer_listener" "listener" {
    default_backend_set_name = oci_load_balancer_backend_set.backend_set.name
    load_balancer_id = oci_load_balancer_load_balancer.load_balancer.id
    name = "app_listener"
    port = 80
    protocol = "HTTP"
}
