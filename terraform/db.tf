# ------ Create an autonomous database
resource "oci_database_autonomous_database" "autonomous_database" {
    admin_password = "Nu6mohk5aiXae2si#"
    compartment_id = var.compartment_ocid
    cpu_core_count = "1"
    data_storage_size_in_tbs = "1"
    db_name = "ADB"
    private_endpoint_label = "ADB"
    db_workload = "OLTP"
    display_name = "ADB"
    subnet_id = oci_core_subnet.private-subnet.id
    nsg_ids = [oci_core_network_security_group.nsg.id]
    license_model = "BRING_YOUR_OWN_LICENSE"
}

# ------ Autonomous DB wallet
resource "oci_database_autonomous_database_wallet" "autonomous_database_wallet" {
  autonomous_database_id = oci_database_autonomous_database.autonomous_database.id
  password               = "Nu6mohk5aiXae2si#"
  base64_encode_content = "true"
}
resource "local_file" "autonomous_database_wallet_file" {
  content_base64 = oci_database_autonomous_database_wallet.autonomous_database_wallet.content
  filename       = "${path.module}/autonomous_database_wallet.zip"
}

