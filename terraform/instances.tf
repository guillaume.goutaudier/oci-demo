# ------ Get a list of Availability Domains
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.compartment_ocid
}

# ------ Get Oracle Linux images
data "oci_core_images" "oracle-linux-images" {
  compartment_id = var.compartment_ocid
  operating_system = "Oracle Linux"
  operating_system_version = "7.8"
  sort_by = "TIMECREATED"
  sort_order = "DESC"
}

# ------ Get Ubuntu images
data "oci_core_images" "ubuntu-images" {
  compartment_id = var.compartment_ocid
  operating_system = "Canonical Ubuntu"
  operating_system_version = "20.04"
  sort_by = "TIMECREATED"
  sort_order = "DESC"
}

# ------ Create bastion virtual machine
resource "oci_core_instance" "bastion" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
    compartment_id = var.compartment_ocid
    shape = "VM.Standard2.1"
    display_name = "bastion"

    create_vnic_details {
        assign_public_ip = "true"
        subnet_id = oci_core_subnet.public-subnet.id
    }
    metadata = {
        ssh_authorized_keys = var.ssh_public_key
        #user_data = base64encode(file(var.custom_bootstrap_file_name))
    }
    source_details {
        source_id = data.oci_core_images.ubuntu-images.images[0].id
        source_type = "image"
    }
}
output "bastion-public-ip" {
  value = oci_core_instance.bastion.public_ip
}

# ------ Create webapp virtual machine
resource "oci_core_instance" "webapp" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
    compartment_id = var.compartment_ocid
    shape = "VM.Standard2.1"
    display_name = "webapp"

    create_vnic_details {
        assign_public_ip = "false"
        subnet_id = oci_core_subnet.private-subnet.id
    }
    metadata = {
        ssh_authorized_keys = var.ssh_public_key
        user_data = base64encode(file("${path.module}/scripts/webapp_init.sh"))
    }
    source_details {
        # Hack: we take the 2d image from the list as the first one is for GPU-based VMs
        # Apparently hack is not required anymore!
        source_id = data.oci_core_images.oracle-linux-images.images[0].id
        source_type = "image"
    }
}
output "webapp-private-ip" {
  value = oci_core_instance.webapp.private_ip
}


