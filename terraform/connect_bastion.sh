export BASTION_IP=$(terraform output -json bastion-public-ip | jq -r)
export WEBAPP_IP=$(terraform output -json webapp-private-ip | jq -r )
ssh -o StrictHostKeyChecking=no -L 127.0.0.1:8022:$WEBAPP_IP:22 ubuntu@$BASTION_IP

